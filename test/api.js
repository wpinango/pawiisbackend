const request = require('supertest');
const app = require('../app');
const expect = require('chai').expect;

describe('Login API', function() {
    it('Deberia ser exitoso con las credenciales correctas', function(done) {
        request(app)
           .post('/api/user/login')
           .set('Accept', 'application/json')
           .set('Content-Type', 'application/json')
           .send({ email: 'roger@alba.com', password: '123456' })
           .expect(200)
           .expect('Content-Type', /json/)
           .expect(function(response) {
              expect(response.body).not.to.be.empty;
              expect(response.body).to.be.an('object');
           })
           .end(done);
    }); 
});