const userController = require('../controllers/userController');
const petController = require('../controllers/petController');
var auth = require('../middleware/custom-auth-middleware');
const multer = require('multer');

const storage = multer.diskStorage({
   destination: (req, file, cb) => {
     cb(null, './app/uploads')
   },
   filename: (req, file, cb) => {
     cb(null, file.originalname)
   }
 })

module.exports = (app) => {

   app.post('/api/mascota', petController.create);
   app.get('/api/mascota/:id', petController.find);
   app.delete('/api/mascota/:id', petController.delete);
   app.patch('/api/mascota/:id', petController.update);
   app.get('/api/mascotas/list/:id', petController.list);
   

   app.post('/api/user/register', userController.create);
   app.post('/api/user/login', userController.login); 
   app.get('/api/user/:id', userController.find);
};