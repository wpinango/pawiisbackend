const users = require('../models').users;

module.exports = async function(req, res, next) {
  const authHeader = req.headers.authorization;
  console.log(req.originalUrl);
  if (req.originalUrl == '/api/user/login' || req.originalUrl == '/api/user/register') {
    next();
  } else {
    if (authHeader) {
      const token = authHeader.split(' ')[1];

      const user = await users.findAll({
        where: {token,}
      })
      if (user != '') {
        req.user = user.User;
        next();
      } else {
        res.status(401).send({message: false});
      }
      
    } else {
      res.status(401).send({message: false});
    }
  }
}

