'use strict';
const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const Usuario = sequelize.define('pets', {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        pet_name: DataTypes.STRING,
        pet_owner_id:DataTypes.INTEGER,
        pet_type: DataTypes.STRING,
        pet_image: DataTypes.STRING,
        pet_owner_name: DataTypes.STRING
    }, {});
    Usuario.associate = function(models) {
        // associations can be defined here
    };
    return Usuario;
};