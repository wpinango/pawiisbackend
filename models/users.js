'use strict';
const bcrypt = require('bcrypt');
const authToken = require('../models').AuthToken;

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('users', {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        name: DataTypes.STRING,
        last_name:DataTypes.STRING,
        email: DataTypes.STRING,
        password: { 
            type: DataTypes.STRING, 
            select: false,
        },
        token: DataTypes.STRING,
    }, {});

    User.associate = function ({ AuthToken }) {
        User.hasMany(AuthToken);
    };

    User.authenticate = async function(email, password) {
        const user = await User.findOne({ where: { email }, },);
        if (bcrypt.compareSync(password, user.password)) {  
            user.password = 0;
            return user.authorize();
        } else {
            console.log('paso');
            throw new Error('invalid password');    
        }
        //throw new Error('invalid password');
    }
    
    
    User.prototype.authorize = async function () {
        try {
            const user = this
            user.update({
                token: bcrypt.hashSync(user.id.toString(), 10)
          });
          return user;
        }catch (e) {
            console.log(e);
            throw new Error('invalid password');
        } 
    };

    User.prototype.logout = async function (token) {
        sequelize.models.AuthToken.destroy({ where: { token } });
      };

    return User;
    
};