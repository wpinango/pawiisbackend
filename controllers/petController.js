const Sequelize = require('sequelize');
const pets = require('../models').pets;
const fs =  require('fs');
var path = require("path");
const bcrypt = require('bcrypt');

function getImagePath(req) {
    var imgData = req.body.pet_image;
    var base64Data = imgData.replace(/^data:image\/png;base64,/, "");
    const filename = req.body.pet_name + Date.now().toString();
    const fullPath = "public/images/" + filename + '.jpg';
    require("fs").writeFile(fullPath, base64Data, 'base64', 
    function(err, data) {
        if (err) {
            console.log('err', err);
        }
        console.log('success');    
    });
    const imagePath = 'http://' + req.headers.host + '/images/' + filename +'.jpg';
    console.log(imagePath); 
    return imagePath.toString();
}

module.exports = {

    async create(req, res) {
        return pets
            .create ({
                pet_name: req.body.pet_name,
                pet_owner_id: req.body.pet_owner_id,
                pet_type: req.body.pet_type,
                pet_image: getImagePath(req), 
                pet_owner_name: req.body.pet_owner_name
            })
            .then(pets => res.status(200).send({
                message: true,
                data: pets,
            }))
            .catch(error => res.status(400).send({message: false}))
    },

    update(req, res) { 
        try {
        var imagePath = '';
        if (req.body.pet_image != null) {
            imagePath = getImagePath(req);
        } else {
            imagePath = req.body.avatar;
        }
        return pets.update({
                pet_name: req.body.pet_name,
                pet_owner_id: req.body.pet_owner_id,
                pet_type: req.body.pet_type,
                pet_image: imagePath.toString(),
                pet_owner_name: req.body.pet_owner_name
            }, {
            where: {
                id: req.params.id,
            }
        })
        .then(pets => res.status(200).send({
            message: true,
        }))
        .catch(error => res.status(400).send({
            message: false,
        }))
        }catch(e) {
            console.log('error : ' + e);
        }
    },
    
    list(req, res) {
        return pets.findAll({
            where: {
                pet_owner_id:req.params.id
            }
        })
        .then(pets => res.status(200).send({
            message: true,
            data: pets
        }))
        .catch(error => res.status(400).send({
            message: false,
        }))
    },
 
    find (req, res) {
        console.log(req.params)
        return pets.findAll({
            where: {
                id: req.params.id,
            }
        })
        .then(pets => res.status(200).send({
            message: true,
            data: pets
        }))
        .catch(error => res.status(400).send({
            message: false,
        }))
    },
    
    delete(req, res) {
        console.log(req.params)
        return pets.destroy({
            where: {
                id: req.params.id,
            }
        })
        .then(pets => res.status(200).send({message: true}))
        .catch(error => res.status(400).send({message: false}))
    }
};