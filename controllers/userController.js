const Sequelize = require('sequelize');
const users = require('../models').users;
const bcrypt = require('bcrypt');

module.exports = {
    create(req, res) {
        const hash = bcrypt.hashSync(req.body.password, 10);
        console.log(req.body);
        console.log(hash);
        return users
            .create ({
                name: req.body.name,
                last_name: req.body.last_name,
                email:req.body.email,
                password: hash,
            })
            .then(users => res.status(200).send({data: users, message: true}))
            .catch(error => res.status(400).send({message: false}))
    },

    login(req, res) {
        return users
            .authenticate (req.body.email, req.body.password)
            .then(users => res.status(200).send({data: users, message: true}))
            .catch(error => res.status(400).send({message: false}))
    },

    list(_, res) {
        return users.findAll({})
            .then(usuario => res.status(200).send({data: users, message: true}))
            .catch(error => res.status(400).send({message: false}))
    },
    
    find (req, res) {
        return users.findAll({
            where: {
                id: req.params.id,
            }
        })
        .then(users => res.status(200).send({data: users, message: true}))
        .catch(error => res.status(400).send({message: false}))
    },
};